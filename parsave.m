function parsave(filename, variable_name)

    varname = genvarname(inputname(2));
    eval([varname ' = variable_name']);

    try
        save(filename, varname, '-append');
    catch
        save(filename, varname);
    end

end
