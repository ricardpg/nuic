% Folder containing the images used to estimate the non-uniform illumination pattern
% StyleInputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\192 2015-06-24T0444Z\Right\4_illumination';
% StyleInputPath = 'N:\Sequences\2016-09-02 - Gorgonies\Images\left_jpeg_photoshop_pattern';
% StyleInputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\Data\Samples\20170309T0019Z\Left\013123Z_background_estimation';
% StyleInputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\Data\20170309T0311Z_rgb\Left\035500Z_ic_pattern';
% StyleInputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\Data\Testing\Left\035500Z_ic_pattern';
StyleInputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\Data\20170309T0311Z_rgb\Right\035500Z_ic_pattern';
% StyleInputPath = 'N:\Sequences\ROBUST\Images';

% Input folder path
% InputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\192 2015-06-24T0444Z\Right\6';
% InputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\192 2015-06-24T0444Z\Test\background';
% InputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\192 2015-06-24T0444Z\Test\Images';
% InputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\192 2015-06-24T0444Z\Test\background';
% InputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\192 2015-06-24T0444Z\Test\Images';
% InputPath = 'N:\Sequences\2016-09-02 - Gorgonies\Images\left_jpeg_photoshop';
% InputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\Data\Samples\20170309T0019Z\Left\013123Z';
% InputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\Data\Samples\20170309T0019Z\Left\013431Z';
% InputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\Data\20170309T0311Z_rgb\Left\035500Z';
% InputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\Data\Testing\Left\035500Z_bg';
InputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\Data\20170309T0311Z_rgb\Right\042619Z';
% InputPath = 'N:\Sequences\ROBUST\Images';

% Output folder path
% OutputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\192 2015-06-24T0444Z\Right\6_ic_HSV';
% OutputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\192 2015-06-24T0444Z\Test\ic_aux';
% OutputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\192 2015-06-24T0444Z\Test\background_ic';
% OutputPath = 'N:\Sequences\2016-09-02 - Gorgonies\Images\left_jpeg_photoshop_illumination_compensated_hsi';
% OutputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\192 2015-06-24T0444Z\Right\6_ic';
% OutputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\Data\Samples\20170309T0019Z\Left\013431Z_ic';
% OutputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\Data\20170309T0311Z_rgb\Left\035500Z_ic';
% OutputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\Data\Testing\Left\035500Z_ic_hsv';
OutputPath = 'N:\Sequences\Deep Vision - 2015-06-24T0444Z\Data\20170309T0311Z_rgb\Right\042619Z_ic_hsv';
% OutputPath = 'N:\Sequences\ROBUST\Images_ic';

% ImageFormat = 'jpg';

% Input image format
% ImageFormat = 'jpg';
ImageFormat = 'png';
% ImageFormat = 'jpg';

% Gamma value of the input images
% Gamma = 1.195;
Gamma = 1;
% Gamma = 2.2;

% Estimate the non-uniform illumination pattern
NUIPattern = UVL_BTB_computeIlluminationPatternHSV(StyleInputPath, ImageFormat, Gamma);

save([StyleInputPath '\NUIPattern - ' datestr(now, 'yyyy-mm-dd - HH-MM-SS') '.mat'], 'NUIPattern');

% Compensate the non-uniform illumination of all the folder images
UVL_BTB_compensateIlluminationHSV(NUIPattern, InputPath, OutputPath, ImageFormat, Gamma);
