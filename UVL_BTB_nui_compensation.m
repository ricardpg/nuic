function varargout = UVL_BTB_nui_compensation(varargin)
% UVL_BTB_NUIC MATLAB code for UVL_BTB_nuic.fig
%      UVL_BTB_NUIC, by itself, creates a new UVL_BTB_NUIC or raises the existing
%      singleton*.
%
%      H = UVL_BTB_NUIC returns the handle to a new UVL_BTB_NUIC or the handle to
%      the existing singleton*.
%
%      UVL_BTB_NUIC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UVL_BTB_NUIC.M with the given input arguments.
%
%      UVL_BTB_NUIC('Property','Value',...) creates a new UVL_BTB_NUIC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before UVL_BTB_nuic_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to UVL_BTB_nuic_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help UVL_BTB_nuic

% Last Modified by GUIDE v2.5 14-Mar-2016 11:45:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @UVL_BTB_nuic_OpeningFcn, ...
                   'gui_OutputFcn',  @UVL_BTB_nuic_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before UVL_BTB_nuic is made visible.
function UVL_BTB_nuic_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to UVL_BTB_nuic (see VARARGIN)

% Choose default command line output for UVL_BTB_nuic
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes UVL_BTB_nuic wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = UVL_BTB_nuic_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function EditTextPatternFolder_Callback(hObject, eventdata, handles)
% hObject    handle to EditTextPatternFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditTextPatternFolder as text
%        str2double(get(hObject,'String')) returns contents of EditTextPatternFolder as a double


% --- Executes during object creation, after setting all properties.
function EditTextPatternFolder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditTextPatternFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in PushButtonPatternFolder.
function PushButtonPatternFolder_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonPatternFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

FolderName = uigetdir('C:\');

if FolderName
    PatternFolderName = FolderName;
    fprintf('Pattern folder name: %s\n', PatternFolderName);
    handles.EditTextPatternFolder.String = PatternFolderName;
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over PushButtonPatternFolder.
function PushButtonPatternFolder_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to PushButtonPatternFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function EditTextInputFolder_Callback(hObject, eventdata, handles)
% hObject    handle to EditTextInputFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditTextInputFolder as text
%        str2double(get(hObject,'String')) returns contents of EditTextInputFolder as a double


% --- Executes during object creation, after setting all properties.
function EditTextInputFolder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditTextInputFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function EditTextOutputFolder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditTextInputFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in PushButtonInputFolder.
function PushButtonInputFolder_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonInputFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

FolderName = uigetdir('C:\');

if FolderName
    InputFolderName = FolderName;
    fprintf('Input folder name: %s\n', InputFolderName);
    handles.EditTextInputFolder.String = InputFolderName;
end


% --- Executes on button press in PushButtonCorrect.
function PushButtonCorrect_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonCorrect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if strcmp(handles.EditTextPatternFolder.String, '   Pattern Folder')
    handles.EditTextStatus.ForegroundColor = [1.0 0.0 0.0];
    handles.EditTextStatus.FontWeight = 'bold';
    handles.EditTextStatus.String = ['   Pattern folder not specified !'];
elseif strcmp(handles.EditTextInputFolder.String, '   Input Folder')
    handles.EditTextStatus.ForegroundColor = [1.0 0.0 0.0];
    handles.EditTextStatus.FontWeight = 'bold';
    handles.EditTextStatus.String = ['   Input folder not specified !'];
elseif strcmp(handles.EditTextOutputFolder.String, '   Output Folder')
    handles.EditTextStatus.ForegroundColor = [1.0 0.0 0.0];
    handles.EditTextStatus.FontWeight = 'bold';
    handles.EditTextStatus.String = ['   Output folder not specified !'];
else
    handles.EditTextStatus.ForegroundColor = [0.0 0.0 0.0];
    
    InputImageFormat = handles.PopupMenuInputImageFormat.String{handles.PopupMenuInputImageFormat.Value};
    OutputImageFormat = handles.PopupMenuInputImageFormat.String{handles.PopupMenuOutputImageFormat.Value};
    
    PatternFolderDir = dir([handles.EditTextPatternFolder.String '\*.' InputImageFormat]);
    InputFolderDir = dir([handles.EditTextInputFolder.String '\*.' InputImageFormat]);
    
    if numel(PatternFolderDir) > 0
        if numel(InputFolderDir) > 0
            handles.EditTextStatus.FontWeight = 'normal';
            handles.EditTextStatus.String = '   Compensating non-uniform illumination ...';
            handles.PushButtonCorrect.Enable = 'off';
            drawnow
            UVL_BTB_correctNonUniformIllumination(handles.EditTextPatternFolder.String, handles.EditTextInputFolder.String, handles.EditTextOutputFolder.String, InputImageFormat, OutputImageFormat, handles.SliderGain.Value);
            handles.PushButtonCorrect.Enable = 'on';
            
            handles.EditTextStatus.String = '   Non-uniform illumination compensation done !';
        end
    end
    
end


% --- Executes during object creation, after setting all properties.
function PushButtonInputFolder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PushButtonInputFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function PushButtonOutputFolder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PushButtonInputFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in PushButtonOutputFolder.
function PushButtonOutputFolder_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonOutputFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

FolderName = uigetdir('C:\');

if FolderName
    OutputFolderName = FolderName;
    fprintf('Output folder name: %s\n', OutputFolderName);
    handles.EditTextOutputFolder.String = OutputFolderName;
end



function EditTextStatus_Callback(hObject, eventdata, handles)
% hObject    handle to EditTextStatus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditTextStatus as text
%        str2double(get(hObject,'String')) returns contents of EditTextStatus as a double


% --- Executes during object creation, after setting all properties.
function EditTextStatus_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditTextStatus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopupMenuInputImageFormat.
function PopupMenuInputImageFormat_Callback(hObject, eventdata, handles)
% hObject    handle to PopupMenuInputImageFormat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopupMenuInputImageFormat contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopupMenuInputImageFormat


% --- Executes during object creation, after setting all properties.
function PopupMenuInputImageFormat_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopupMenuInputImageFormat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function SliderGain_Callback(hObject, eventdata, handles)
% hObject    handle to SliderGain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

handles.EditTextGain.String = num2str(handles.SliderGain.Value);

% --- Executes during object creation, after setting all properties.
function SliderGain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SliderGain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function EditTextGain_Callback(hObject, eventdata, handles)
% hObject    handle to EditTextGain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditTextGain as text
%        str2double(get(hObject,'String')) returns contents of EditTextGain as a double


% --- Executes during object creation, after setting all properties.
function EditTextGain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditTextGain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopupMenuOutputImageFormat.
function PopupMenuOutputImageFormat_Callback(hObject, eventdata, handles)
% hObject    handle to PopupMenuOutputImageFormat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopupMenuOutputImageFormat contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopupMenuOutputImageFormat


% --- Executes during object creation, after setting all properties.
function PopupMenuOutputImageFormat_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopupMenuOutputImageFormat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
