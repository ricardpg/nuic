function NUIPattern = UVL_BTB_computeIlluminationPatternHSV(StyleInputPath, ImageFormat, Gamma)

    % Style Input Path Verification
    if exist(StyleInputPath, 'dir')
        InputDir = dir([StyleInputPath '/*.' ImageFormat]);
    else
        fprintf('Directory %s not found !\n', StyleInputPath);
        return
    end

    n_images = numel(InputDir);

    % Images Information Obtention
    info = imfinfo([StyleInputPath '/' InputDir(1).name]);
    NUIPattern = zeros(info.Height, info.Width, 'double');

    % Image Sequence Processing
    for i = 1 : n_images

        fprintf('Processing Image %5d / %5d : %s ...\n', i, n_images, [StyleInputPath '/' InputDir(i).name]);
        fprintf('Converting image to linear space using Gamma = %1.3f ...\n', Gamma);

        I_lin = double(imread([StyleInputPath '/' InputDir(i).name], ImageFormat)) / (double(2^(info.BitDepth / 3) - 1));
        I_lin = I_lin.^Gamma;

        fprintf('Converting image to grayscale ...\n');
        I_hsv = rgb2hsv(I_lin);
        I_lin = I_hsv(:, :, 3);
        
        fprintf('Image values range [%5d %5d] ...\n', min(I_lin(:)), max(I_lin(:)));
        
        NUIPattern = NUIPattern + I_lin;

    end

    NUIPattern = NUIPattern / n_images;

end
