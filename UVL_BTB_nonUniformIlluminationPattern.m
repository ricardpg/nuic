function NUIPattern = UVL_BTB_nonUniformIlluminationPattern(StyleInputPath, ImageFormat)

    % Style Input Path Verification
    if exist(StyleInputPath, 'dir')
        InputDir = dir([StyleInputPath '/*.' ImageFormat]);
    else
        fprintf('Directory %s not found !\n', StyleInputPath);
        return
    end

    n_images = size(InputDir, 1);

    % Images Size Obtention
    [I_height, I_width, I_depth] = size(imread([StyleInputPath '/' InputDir(1).name], ImageFormat));
    NUIPattern = zeros(I_height, I_width, 'double');

    % Image Sequence Processing
    for i = 1 : n_images;

        fprintf('Processing Image %5d / %5d : %s ...\n', i, n_images, [StyleInputPath '/' InputDir(i).name]);
        I_lin = single(imread([StyleInputPath '/' InputDir(i).name], ImageFormat)) / 255;

        if I_depth == 3

            fprintf('Converting image to grayscale ...\n');
            I_lin = rgb2gray(I_lin);
            NUIPattern = NUIPattern + I_lin;

        elseif I_depth == 1

            fprintf('The Input Images are not in Color !\n');
            NUIPattern = NUIPattern + I_lin;

        end

    end

    NUIPattern = NUIPattern / n_images;
    
end
