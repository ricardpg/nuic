function UVL_BTB_compensateIlluminationHSV(Pattern, InputPath, OutputPath, ImageFormat, Gamma)

    if exist(InputPath, 'dir')
        InputDir = dir([InputPath '/*.' ImageFormat]);
    else
        fprintf('Folder %s not found !\n', InputPath);
        return
    end
    
    if ~exist(OutputPath, 'dir')
        mkdir(OutputPath);
    end
    
    n_images = numel(InputDir);

    % Images Information Obtention
    info = imfinfo([InputPath '/' InputDir(1).name]);
    fprintf('Input images are %s ...\n', info.ColorType);

    IFlat = ones(size(Pattern)) * max(max(Pattern)) - Pattern;  % Test: Additive factor
    IMult = ones(size(Pattern)) * max(max(Pattern)) ./ Pattern; % Test: Multiplicative factor
    
    % Image Sequence Processing
    for i = 1 : n_images

        fprintf('Processing Image %5d / %5d : %s ...\n', i, n_images, [InputPath '/' InputDir(i).name]);
        fprintf('Converting image to linear space using Gamma = %1.3f ...\n', Gamma);

        I_lin = double(imread([InputPath '/' InputDir(i).name], ImageFormat)) / (double(2^(info.BitDepth / 3) - 1));
        I_lin = I_lin.^Gamma;
        
        I_lin_hsv = rgb2hsv(I_lin);

%         % Subtractive
%         Pattern_inv = 1 - Pattern;
%         Pattern_inv = Pattern_inv - min(Pattern_inv(:));
% 
%         I_lin_hsv_v_ic = I_lin_hsv(:, :, 3) + Pattern_inv;
% 
%         I_lin_hsv_ic(:, :, 1 : 2) = I_lin_hsv(:, :, 1 : 2);
%         I_lin_hsv_ic(:, :, 3) = I_lin_hsv_v_ic;
%         I_ic = hsv2rgb(I_lin_hsv_ic);

        % Product
        I_lin_hsv_ic(:, :, 1 : 2) = I_lin_hsv(:, :, 1 : 2);
        I_lin_hsv_ic(:, :, 3) = I_lin_hsv(:, :, 3) .* IMult;
%         I_lin_hsv_ic(:, :, 3) = I_lin_hsv(:, :, 3) + IFlat;
        I_ic = hsv2rgb(I_lin_hsv_ic);

        fprintf('Storing image %s...\n', [OutputPath '/' 'lin_' InputDir(i).name(1 : end - 4) '.png']);
        I_temp = uint8(I_ic * (double(2^(info.BitDepth / 3)) - 1));

        imwrite(I_temp, [OutputPath '/' InputDir(i).name(1 : end - 4) '.png'], 'PNG');
        fprintf('Image values range [%5d %5d] ...\n', min(I_temp(:)), max(I_temp(:)));

    end

end
