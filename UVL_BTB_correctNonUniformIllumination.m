function UVL_BTB_correctNonUniformIllumination(PatternInputPath, InputPath, OutputPath, InputImageFormat, OutputImageFormat, Gain)

    if exist(PatternInputPath, 'dir')
        Pattern = UVL_BTB_nonUniformIlluminationPattern(PatternInputPath, InputImageFormat);
        fprintf('\n');
    else
        fprintf('Pattern images folder %s not found !\n', StyleInputPath);
        return
    end

    if exist(InputPath, 'dir')
        InputDir = dir([InputPath '/*.' InputImageFormat]);
    else
        fprintf('Folder %s not found !\n', InputPath);
        return
    end
    
    if ~exist(OutputPath, 'dir')
        mkdir(OutputPath);
    end

    if nargin < 5
        Gain = 2.25;
    end
    
    n_images = size(InputDir, 1);
    
    fprintf('Computing Illumination Pattern ...\n\n');
    I = imread([InputPath '/' InputDir(1).name], InputImageFormat);
    [I_height, I_width, I_depth] = size(I);

    PatternNormGauss = Pattern;

    parfor i = 1 : n_images;

        fprintf('Processing Image %5d / %5d : %s ...\n', i, n_images, [InputPath '/' InputDir(i).name]);
        I_lin = single(imread([InputPath '/' InputDir(i).name], InputImageFormat)) / 255;

        if I_depth == 3

            I_ic = I_lin ./ repmat(PatternNormGauss, [1 1 3]);
            I_ic = mat2gray(I_ic, [0 Gain]); % [0 10]

            if strcmpi(OutputImageFormat, 'mat')

                Image = I_ic;
                parsave([OutputPath '/' InputDir(i).name(1 : end - 4) '.' InputImageFormat], Image);

            else

                imwrite(I_ic, [OutputPath '/' InputDir(i).name(1 : end - 4) '.' InputImageFormat], InputImageFormat);

            end
            
        elseif I_depth == 1
            
            fprintf('The Input Imagesa are not in Color !\n');

        end
        
        fprintf('Done !\n');

    end
end
